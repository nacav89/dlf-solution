#!/usr/bin/env python3

import time
from math import *

import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Image
from sensor_msgs.msg import Range
from hector_uav_msgs.srv import EnableMotors

import cv2
import numpy as np
from cv_bridge import CvBridge, CvBridgeError
  
x = 0
y = 0

def atan2_fixed(x,y):
    u=atan2(x,y)
    if u <= 2*pi:
        return u+2*pi
    elif u>= pi:
        return u- 2*pi
    else:
        return u
#========Saturation=================
def saturation(U, pre):
    if U < 0:
        return -1 * min(abs(U), pre)

    elif U > 0:
        return 1 * min(abs(U), pre)

    else:
        return 0

#=========================Обработчик изображения===========================
def skeletonize(img):
    try:
        """ Скелетонизация изображения """
        if img is None: 
            print('Ошибка скелетонизации ')
            return None
        skel = np.zeros(img.shape, np.uint8)
        size = np.size(img)
        element = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))
        done = False

        while not done:
            eroded = cv2.erode(img, element)
            temp = cv2.dilate(eroded, element)
            temp = cv2.subtract(img, temp)
            skel = cv2.bitwise_or(skel, temp)
            img = eroded.copy()

            zeros = size - cv2.countNonZero(img)
            if zeros == size:
                done = True

        return skel
    except:
        return None

def line_detection(image):            
    if image is None:
        print ('Error image!')
        return -1
    
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV) 
    dst = cv2.Canny(image, 50, 200, None, 3)
    # Copy edges to the images that will display the results in BGR
    cdst = cv2.cvtColor(dst, cv2.COLOR_GRAY2BGR)
    img_counters=image.copy()
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    image = cv2.medianBlur(image,7)

    cdstP =  cv2.adaptiveThreshold (image,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
    cdstP = cv2.bitwise_not(cdstP)

    _,alpha = cv2.threshold(cdstP,0,255,cv2.THRESH_BINARY)
    # Threshold of blue in HSV space
    
    lower_gray = np.array([0, 0, 0]) 
    upper_gray = np.array([0, 0, 40]) 
  
    # preparing the mask to overlay 
    mask = cv2.inRange(hsv, lower_gray, upper_gray) 
      
    # The black region in the mask has the value of 0, 
    # so when multiplied with original image removes all non-blue regions 
    result = cv2.bitwise_and(hsv, hsv, mask = mask) 
  
    #Copy edges to the images that will display the results in BGR
    mask = cv2.fastNlMeansDenoising(mask,100)





# Инвертирование изображения для выделения черных линий на белом фоне
    inverted = mask
    inverted = cv2.medianBlur(inverted,7)
    # Применение порогового преобразования для выделения черных линий
    _, thresh = cv2.threshold(inverted, 50, 255, cv2.THRESH_BINARY) #?
    # Нахождение контуров
    contours, hierctory  = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    sorted_contours = sorted(contours, key=cv2.contourArea, reverse=True)
    # Проверка, есть ли хотя бы два контура
    if len(sorted_contours) !=0:
        if len(sorted_contours) >= 2:
            # Выбор двух самых больших контуров
            largest_contour = sorted_contours[0]
            second_largest_contour = sorted_contours[1]
            all_points = np.concatenate((largest_contour, second_largest_contour))
            hull = cv2.convexHull(all_points)
            cv2.drawContours(img_counters, [largest_contour], -1, (0, 255, 0), 3)
            cv2.drawContours(img_counters, [second_largest_contour], -1, (255, 0, 0), 3)
            cv2.drawContours(img_counters, [hull], -1, (255,255,0),5)
            x, y, w, h = cv2.boundingRect(hull)

        elif len(sorted_contours) ==1:
            largest_contour = sorted_contours[0]
            x, y, w, h = cv2.boundingRect(largest_contour)
        w_max, h_max = image.shape[:2]    
        cropped_image=thresh[x:saturation(x+w,w_max),y:saturation(y+h,h_max)]
        skeleton = skeletonize(cropped_image)
        # Вывод изображения с нарисованными контурами
        if skeleton is not None:
            cv2.imshow('Skeleton', skeleton)
        cv2.imshow('counters', img_counters)
        cv2.imshow('Mask', mask)
        return skeleton
    else:   
         
        print('Не найдено достаточно контуров.')
        return None
def line_follower(mask):
    global x,y
    if mask is None:
        return x*-0.9,y*-1
    height, width = mask.shape[:2]
    # Нахождение координат всех ненулевых пикселей в маске
    coordinates = cv2.findNonZero(mask)
    if coordinates is None:
        return x*-1,y*-1
    # Вывод координат пикселей
    for point in coordinates:
        locx, locy = point[0]
        if  locy/height > 0.1 and locy/height < 0.5:
            #print(f"Координаты пикселя: ({x}, {y})")
            return int(locx-width//2),int(locy-height//2)
    else:
        for point in coordinates:
            locx, locy = point[0]
            return int(locx-width//2),int(locy-height//2)
        else:
            return  x*-1,y*-1

#========Регулятор===============

def get_angle(x, y):
    angle = atan2(y, x)
    return angle



def reg(x, y):
    dist_err = sqrt(x*x + y*y)
    angle_err = atan2(y, x)
    dist_U = saturation(0.3* dist_err,1)
    angle_U =saturation(0.37* angle_err,35)
    return dist_U, angle_U

def reg_height(heigth, end_heigth):
    lz=(end_heigth-heigth)*0.4
    return lz

#ИНТЕРФЕЙС ОТЛАДКИ==========================
def update():
    global x, y
    return x, y

def update_lx(v):
    global x
    x = v

def update_az(v):
    global y
    y = v
#=============================


class SimpleMover():

    def __init__(self):
        rospy.init_node('simple_mover', anonymous=True)
        if rospy.has_param('gui'):
            self.enabled_gui = rospy.get_param('gui')
        else:
            rospy.logerr("Failed to get param 'gui'")
        self.cv_bridge = CvBridge()
        self.Image1 = None
        self.Image2 = None
        self.cmd_vel_pub = rospy.Publisher('cmd_vel', Twist, queue_size=1)
        rospy.Subscriber("cam_1/camera/image", Image, self.camera_cb)
        rospy.Subscriber("cam_2/camera/image", Image, self.camera_cb2)
        rospy.Subscriber("sonar_height", Range, self.range_callback)
        self.rate = rospy.Rate(30)
        rospy.on_shutdown(self.shutdown)


    def camera_cb(self, msg):
        try:
            cv_image = self.cv_bridge.imgmsg_to_cv2(msg, "bgr8")
            self.Image1 = cv_image
        except CvBridgeError as e:
            rospy.logerr("CvBridge Error: {0}".format(e))

    def camera_cb2(self, msg):
        try:
            cv_image2 = self.cv_bridge.imgmsg_to_cv2(msg, "bgr8")
            self.Image2 = cv_image2
        except CvBridgeError as e:
            rospy.logerr("CvBridge Error: {0}".format(e))
    def range_callback(self,data):
        self.range = data.range

    def enable_motors(self):

        try:
            rospy.wait_for_service('enable_motors', 2)
            call_service = rospy.ServiceProxy('enable_motors', EnableMotors)
            response = call_service(True)
        except Exception as e:
            print("Error while try to enable motors: ")
            print(e)


    def take_off(self):

        self.enable_motors()

        start_time = time.time()
        end_time = start_time + 4
        twist_msg = Twist()
        twist_msg.linear.z = 0.4

        while (time.time() < end_time) and (not rospy.is_shutdown()):
            self.cmd_vel_pub.publish(twist_msg)
            self.rate.sleep()



    def spin(self):
        global x, y
        self.take_off()

        start_time = time.time()
        while not rospy.is_shutdown():
            twist_msg = Twist()
            t = time.time() - start_time

            dist_U, angle_U = reg(x, y)
            if self.range >2.8:
                twist_msg.linear.z = reg_height(self.range, 2.5)
                twist_msg.linear.x = dist_U/2
                twist_msg.angular.z = angle_U/2
            else:
                twist_msg.linear.z = reg_height(self.range, 2.7)
                twist_msg.linear.x = dist_U
                twist_msg.angular.z = angle_U
            self.cmd_vel_pub.publish(twist_msg)
            self.rate.sleep()

            if self.enabled_gui:
                if self.Image1 is not None and self.Image2 is not None:
                    print(x,y,end='=')
                    print(dist_U,reg_height(self.range, 1),angle_U,end='=')
                    print(self.range)
                    y,x=line_follower(line_detection(self.Image1))
                    y*=-1
                    x*=-1
                    cv2.createTrackbar( "x", "Test regulator", x, 7, update_lx )
                    cv2.createTrackbar( "y", "Test regulator", y, 7, update_az )
                    cv2.imshow("Test regulator", self.Image1)
                    cv2.imshow("Front view camera from Robot", self.Image2)
    
                    cv2.waitKey(3)



    def shutdown(self):
        self.cmd_vel_pub.publish(Twist())
        rospy.sleep(1)


simple_mover = SimpleMover()
simple_mover.spin()
